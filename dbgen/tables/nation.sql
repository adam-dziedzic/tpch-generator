CREATE TABLE nation (
n_nationkey BIGINT NOT NULL,
n_name CHAR(25) NOT NULL,
n_regionkey BIGINT NOT NULL,
n_comment CHAR(152) NOT NULL
);
